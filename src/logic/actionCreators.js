import types from './actionTypes';

export const addItem = (content) => {
  console.log('content', content);
  return { type: types.ADD_ITEM, content };
};

export const removeItem = (id) => {
  return { type: types.REMOVE_ITEM, id };
};

export const toggleItem = (id) => {
  return { type: types.TOGGLE_ITEM, id };
};

export const hideCompletedItem = () => {
  return { type: types.HIDE_COMPLETED_ITEM};
};