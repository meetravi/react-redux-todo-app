import types from './actionTypes';

export const initialState = {
  items: [
    { id: 1, content: 'Call mum', completed: false },
    { id: 2, content: 'Buy cat food', completed: false },
    { id: 3, content: 'Water the plants', completed: false },
  ],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.ADD_ITEM:
      const nextId =
        state.items.reduce((id, item) => Math.max(item.id, id), 0) + 1;
      const newItem = {
        id: nextId,
        content: action.content,
        completed: false,
      };

      return {
        ...state,
        items: [...state.items, newItem],
      };

    case types.REMOVE_ITEM:
      return {
        items: state.items.filter(item =>
          item.id !== action.id
        ),
      }

    case types.TOGGLE_ITEM:
      return {
        items: state.items.map(item =>
          (item.id === action.id)
            ? {...item, completed: !item.completed}
            : item
        ),
      };

    case types.HIDE_COMPLETED_ITEM:
      return {
        items: state.items.filter(item =>
          !item.completed
        ),
      }
    default:
      return state;
  }
};

export default reducer;
