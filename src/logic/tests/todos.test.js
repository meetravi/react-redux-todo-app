import reducer, { initialState } from '../todos';

describe('reducer', () => {
  it('should return state for unknown action', () => {
    const mockState = { test: 'testItem' };
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(mockState, mockAction);
    expect(result).toEqual(mockState);
  });

  it('should use initial state if state not provided', () => {
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(undefined, mockAction);
    expect(result).toEqual(initialState);
  });

  it('should add new items on ADD_ITEM', () => {
    const state = {
      items: [
          { id: 1, content: 'first', completed: false },
          { id: 2, content: 'second', completed: false },
        ],
    };
    const mockAction = { type: 'ADD_ITEM', content: 'third' };
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(3);
    expect(result.items[2].id).toEqual(3);
    expect(result.items[2].content).toEqual('third');
  });

  it('should remove the item on REMOVE_ITEM', () => {
    const state = {
      items: [
          { id: 1, content: 'first', completed: false },
          { id: 2, content: 'second', completed: false },
        ],
    };
    const mockAction = { type: 'REMOVE_ITEM', id: 2 };
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(1);
  });

  it('should toggle the state on TOGGLE_ITEM', () => {
    const state = {
      items: [
          { id: 1, content: 'first', completed: false },
          { id: 2, content: 'second', completed: false },
        ],
    };
    const mockAction = { type: 'TOGGLE_ITEM', id: 2 };
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(2);
    expect(result.items[1].completed).toEqual(true);
  });

  it('should hide the completed tasks on HIDE_COMPLETED_ITEM', () => {
    const state = {
      items: [
          { id: 1, content: 'first', completed: false },
          { id: 2, content: 'second', completed: true },
          { id: 3, content: 'second', completed: true },
        ],
    };
    const mockAction = { type: 'HIDE_COMPLETED_ITEM' };
    const result = reducer(state, mockAction);
    expect(result.items).toHaveLength(1);
  });

});
