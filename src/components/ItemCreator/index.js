import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addItem, hideCompletedItem } from '../../logic/actionCreators';
import classNames from 'classnames';
import './styles.css';

export class ItemCreator extends React.Component{
  render(){
    const { onAdd, hideCompletedItem } = this.props;
    let inputField;
    return (
      <div className="itemCreator">
        <input
          ref={(input) => {
            inputField = input;
          }}
          className="itemCreator-input"
          type="text"
          placeholder="What do you need to do?"
        />
        <input
          className={classNames(
            'itemCreator-button',
            'add'
          )}
          type="button"
          value="Add Task"
          onClick={() => {
            inputField.value && onAdd(inputField.value);
            inputField.value = '';
          }}
        />
        <input
          className={classNames(
            'itemCreator-button',
            'hide'
          )}
          type="button"
          value="Hide Completed"
          onClick={hideCompletedItem}
        />
      </div>
    );
  }
};

ItemCreator.propTypes = {
  onAdd: PropTypes.func.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  onAdd: (newItem) => dispatch(addItem(newItem)),
  hideCompletedItem: () => dispatch(hideCompletedItem())
});

export default connect(null, mapDispatchToProps)(ItemCreator);
