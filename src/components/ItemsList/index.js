import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { removeItem, toggleItem } from '../../logic/actionCreators';
import iconDelete from '../../assets/images/icon-delete.png';
import classNames from 'classnames';
import './styles.css';

export class ItemsList extends React.Component {

  constructor(props) {
    super(props);
    this.handleRemoveItem = this.handleRemoveItem.bind(this);
    this.handleToggleItem = this.handleToggleItem.bind(this);
  }

  handleRemoveItem = (e, item) => {
    const { onRemove } = this.props;
    onRemove(item.id);
  }

  handleToggleItem = (e, item) => {
    const { onToggle } = this.props;
    onToggle(item.id);
  }
  render() {
    const { items } = this.props;
    return (
      <div>
        <ul className="itemsList-ul">
          {items.length < 1 && <p id="items-missing">Add some tasks above.</p>}
          {
            items.map((item) => {
            return (
              <li key={item.id} >
              <input type="checkbox" className="checkbox" onClick={(e) => this.handleToggleItem(e, item)}/>
                <span className={classNames(
                  'content',
                  { 'completed': item.completed }
                )}>{item.content}</span>
                <img className='icon'onClick={(e) => this.handleRemoveItem(e, item)} src={iconDelete} alt="delete" />
              </li>
            )
            })
          }
        </ul>
      </div>
    );
  }

};

ItemsList.propTypes = {
  items: PropTypes.array.isRequired,
  onRemove: PropTypes.func,
};

const mapStateToProps = (state) => {
  return { items: state.todos.items };
};

const mapDispatchToProps = (dispatch) => ({
  onRemove: (item) => dispatch(removeItem(item)),
  onToggle: (item) => dispatch(toggleItem(item)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);
